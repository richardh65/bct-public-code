


To build the ace base image with access to the appropriate ACE install:

sudo docker build -t ace --build-arg DOWNLOAD_URL=http://public.dhe.ibm.com/ibmdl/export/pub/software/websphere/integration/12.0.4.0-ACE-LINUX64-DEVELOPER.tar.gz --file ./Dockerfile .

Then run the seperate image with the extra setup (java, ant, sonar-runner and the mqclient) run"

sudo docker build --tag bct-ace:latest .
sudo docker tag bct-ace:latest richardh65/bct-tools:v7
sudo docker push richardh65/bct-tools:v7

We can then the pre-packaged tests
sudo docker run --network="host" bct-ace:latest



Network host lets the container get out from the docker network to the host network to find the Sonarqube instance.

To start Sonarqube:

sudo docker build --tag bct-sonarqube:latest --file ./SonarqubeDockerfile .

sudo docker run -d -p 9000:9000 -v /home/richard/dockerwork/data:/opt/sonarqube/data -v /home/richard/dockerwork/logs:/opt/sonarqube/logs  bct-sonarqube:latest

It will run against the local instance:
http://127.0.0.1:9000

But you can update the sonar.properties file to use a different host.

Based on:
https://www.ibm.com/docs/en/app-connect/containers_cd?topic=obtaining-app-connect-enterprise-server-image-from-cloud-container-registry
https://github.com/ot4i/ace-docker
https://community.ibm.com/community/user/integration/blogs/amar-shah1/2022/01/05/moving-an-integration-that-uses-ibm-mq-onto-contai


Installing MQ on Linux based on:
https://uplinktv.github.io/ibmmq-devops/
https://www.ibm.com/docs/en/ibm-mq/9.0?topic=rpm-installing-mq-client-linux
https://www.ibm.com/docs/en/ibm-mq/7.5?topic=server-installing-websphere-mq-linux-ubuntu



Other versions here:
https://public.dhe.ibm.com/ibmdl/export/pub/software/websphere/messaging/mqadv/



