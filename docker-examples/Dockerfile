# FROM registry.access.redhat.com/ubi8/ubi as builder
# RUN microdnf update && microdnf install java-1.8.0-openjdk-headless-1:1.8.0.332.b09-2.el8_6.x86_64
# RUN echo test2
# FROM ibmcom/ace:11.0.0.6.3-amd64
# docker pull ibmcom/ace:11.0.0.6.3-amd64
FROM ace:latest

# switch back to root for the installs
USER root

# install tar and zip
RUN microdnf update && microdnf install tar unzip

# add fonts
# java.lang.UnsatisfiedLinkError: /usr/local/sonar-scanner/sonar-scanner-4.7.0.2747-linux/jre/lib/libfontmanager.so: libfreetype.so.6: cannot open shared object file: No such file or directory
RUN microdnf update && microdnf install freetype fontconfig

# Download a jdk
RUN curl https://builds.openlogic.com/downloadJDK/openlogic-openjdk/8u262-b10/openlogic-openjdk-8u262-b10-linux-x64.tar.gz -o /tmp/jdk.tar.gz
RUN mkdir -p /usr/lib/jvm/jdk8
RUN cd /usr/lib/jvm/jdk8 && tar zxvf /tmp/jdk.tar.gz

# setup java path
ENV JAVA_HOME /usr/lib/jvm/jdk8/openlogic-openjdk-8u262-b10-linux-64
ENV PATH $PATH:$JAVA_HOME/bin

# download ANT
# https://dlcdn.apache.org/ant/binaries/apache-ant-1.9.16-bin.zip
# RUN curl https://dlcdn.apache.org//ant/binaries/https://dlcdn.apache.org/ant/binaries/apache-ant-1.9.16-bin.tar.gz -o /tmp/ant1.9.tar.gz
# RUN cd /usr/local/ant && tar zxvf /tmp/ant1.9.tar.gz
# RUN ANT_HOME=/usr/local/ant/apache-ant-1.10.12

RUN mkdir -p /usr/local/ant
COPY installs/apache-ant-1.9.16-bin.zip /tmp/apache-ant-1.9.16-bin.zip
RUN cd /usr/local/ant && unzip /tmp/apache-ant-1.9.16-bin.zip
RUN ANT_HOME=/usr/local/ant/apache-ant-1.9.16

ENV PATH $PATH:$ANT_HOME/bin

# download sonar-scanner
RUN curl https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.7.0.2747-linux.zip -o /tmp/scanner.zip
RUN mkdir -p /usr/local/sonar-scanner
RUN cd /usr/local/sonar-scanner && unzip  /tmp/scanner.zip
RUN SCANNER_HOME=/usr/local/sonar-scanner
ENV PATH $PATH:$SCANNER_HOME/bin

# make ANT runnable
RUN chmod +x /usr/local/ant/apache-ant-1.9.16/bin/ant


# based on
# https://uplinktv.github.io/ibmmq-devops/
# Linux version for redhat/ubi
RUN curl https://public.dhe.ibm.com/ibmdl/export/pub/software/websphere/messaging/mqadv/mqadv_dev930_linux_x86-64.tar.gz -o /tmp/mqavd.tar.gz

RUN mkdir -p /tmp/mqsetup/mqadv
RUN cd /tmp/mqsetup/mqadv && tar zxvf /tmp/mqavd.tar.gz

# accept the licence
RUN cd /tmp/mqsetup/mqadv/MQServer &&  ./mqlicense.sh -text_only -accept

RUN ls -la /tmp/mqsetup/mqadv/MQServer

# install the client rpm's
RUN cd /tmp/mqsetup/mqadv/MQServer && rpm -Uvh MQSeriesAMQP-*.rpm MQSeriesAMS-*.rpm MQSeriesBCBridge-*.rpm MQSeriesClient-*.rpm MQSeriesGSKit-*.rpm MQSeriesJava-*.rpm MQSeriesJRE-*.rpm MQSeriesRuntime-*.rpm MQSeriesSamples-*.rpm MQSeriesSDK-*.rpm MQSeriesServer-*.rpm MQSeriesSFBridge-*.rpm MQSeriesXRService-*.rpm

RUN chown -R aceadmin:aceadmin  /opt/ibm

# switch back to ace user
USER 1001

# setup the sample ACE project and scripts
RUN mkdir -p /tmp/work/bct/code
COPY --chown=aceadmin:aceadmin bct/startup.sh /tmp/work/bct/code/startup.sh
RUN chmod +x /tmp/work/bct/code/startup.sh


RUN mkdir -p /tmp/work/bct/lib
COPY bct/libs /tmp/work/bct/lib


COPY --chown=aceadmin:aceadmin bct/trace_build.xml /tmp/work/bct/code/trace_build.xml
COPY --chown=aceadmin:aceadmin bct/instrumentation_build.xml /tmp/work/bct/code/instrumentation_build.xml

RUN mkdir -p /tmp/work/bct/code/bars
COPY bct/bars/SimplePolicyTestable.bar /tmp/work/bct/code/bars/SimplePolicyTestable.bar

RUN mkdir -p /tmp/work/bct/code/TestSimple_Project
COPY bct/TestSimple_Project /tmp/work/bct/code/TestSimple_Project

RUN mkdir -p /tmp/work/bct/code/TestSimple_Project_Test
COPY bct/TestSimple_Project_Test /tmp/work/bct/code/TestSimple_Project_Test

RUN mkdir -p /tmp/work/bct/code/config
COPY --chown=aceadmin:aceadmin bct/Config /tmp/work/bct/code/config

# property file for scan
COPY --chown=aceadmin:aceadmin bct/sonar-project-tracing.properties /tmp/work/bct/code/sonar-project-tracing.properties
COPY --chown=aceadmin:aceadmin bct/sonar-project-instrumented.properties /tmp/work/bct/code/sonar-project-instrumented.properties


WORKDIR /tmp/work/bct/code

ENTRYPOINT ["bash", "-c", "./startup.sh"]



