echo ================================
echo maven_endtrace
echo ================================

echo Sleeping before finishing trace
timeout /t 2
echo Sleeping before finishing trace....done 

echo mqsireportresourcestats
	
mqsireportresourcestats CoverageNode -e CN_EG2 -d 2 -v statstracefile2.txt

echo ================================
echo Generate logs (xml)
echo ================================
	
mqsireadlog CoverageNode -u -e CN_EG2 -o trace_file\usertrace.xml

echo ================================
echo Generate logs (txt)
echo ================================
	
mqsiformatlog -i trace_file\usertrace.xml -o trace_file\usertrace.txt

echo ================================
echo Turn off tracing
echo ================================
	
mqsichangetrace CoverageNode -u -e CN_EG2 -l none -c 20000 -r

echo ================================
echo Consume trace file / Creating coverage file
echo ================================

java -cp "C:\utils\bct\*" au.com.bettercodingtools.sonar.messagebrokersonar.standalone.ConsumeTraceStandAlone coveragetemp N trace_file\usertrace.txt TestCoverage
