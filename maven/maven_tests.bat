echo ================================
echo maven_tests
echo ================================

echo Sleeping at start of test...
timeout /t 2
echo Sleeping at start of test...done
	
echo Testing Q1
echo java -cp "C:\utils\bct\*" au.com.bettercodingtools.sonar.messagebrokersonar.standalone.SendMQTestMessageStandAlone Q1 OUT testqm1 test.channel 1414 FALSE testdata\test1.txt

echo Testing Q2
echo java -cp "C:\utils\bct\*" au.com.bettercodingtools.sonar.messagebrokersonar.standalone.SendMQTestMessageStandAlone Q2 PGOFF testqm1 test.channel 1414 FALSE testdata\test1.txt

echo ==================================== maven tests called done ==================================================