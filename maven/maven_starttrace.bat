
rem has to run only once 
rem C:/"Program Files"/IBM/IIB/10.0.0.5/server/bin/mqsiprofile.cmd

echo ================================
echo maven_starttrace
echo ================================

echo ================================
echo turn on tracing but clear the current trace
echo ================================
	
mqsichangetrace CoverageNode -u -e CN_EG2 -l debug -c 20000 -r

echo running mqsichangeresourcestats (inactive)
			
mqsichangeresourcestats CoverageNode -e CN_EG2 -c inactive
	
echo running mqsichangeresourcestats (active)
	
mqsichangeresourcestats CoverageNode -e CN_EG2 -c active 

echo running mqsichangeflowstats

mqsichangeflowstats CoverageNode -e CN_EG2 -j -a -c active